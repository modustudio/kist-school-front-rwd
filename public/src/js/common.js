$(function(){
  setTimeout(function(){
    $('.wrapper').animate({'opacity':'1'});
    $(window).trigger('resize');
  },50);
});

$(window).on("load resize", function () {
  if (navigator.userAgent.match(/Android|Mobile|iP(hone|od|ad)|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune/)) {
    $('body').addClass('device-mobile')
  } else {
    $('body').removeClass('device-mobile')
  }
  if ($(document).width() <= 1024) {
    $('body').addClass('media-mobile');
  } else {
    $('body').removeClass('media-mobile');
  }
}).resize();

// GNB
$(function () {
  var gnb = '.site-gnb';
  var gnbList = '.site-gnb__depth1-list';
  var gnbItem = '.site-gnb__depth1-item';
  var gnbLink = '.site-gnb__depth1-link';
  var header = '.site-header';
  var logo = '.top-logo';
  var gnbState = 0;
  var siteMap = '.site-map';
  var siteMapBtn = '.site-map-btn';

  $(gnb + ">ul>li>a").on("click mouseenter focus touchstart", function (e) {
    if (!$('body').hasClass('media-mobile')) {
      if (!$(this).parents("li").hasClass("js-open-d")) {
        $(gnb).addClass('js-open-d');
        $(logo).addClass('js-open-d');
        $(siteMapBtn).addClass('js-black');
        $(this).parents("li").addClass("js-open-d").siblings("li").removeClass("js-open-d js-first");
        if (gnbState === 0) {
          $(this).parents("li").addClass("js-first");
          gnbState = 1;
        }
        if($(this).parents("li").hasClass('child-none')){
          $(gnb).removeClass('js-open-d');
          $(logo).removeClass('js-open-d');
          $(siteMapBtn).removeClass('js-black');
          gnbState = 0;
        }
      }
      if (e.type === "touchstart") {
        return false;
      }
    } else if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) {
      if (e.type === "click") {
        if ($(this).closest(gnbItem).find('ul').length == 0) return;
        e.preventDefault();
        if ($(this).closest(gnbItem).hasClass('is-over')) {
          $(this).closest(gnbItem).removeClass('is-over');
        } else {
          $(this).closest(gnbItem).addClass('is-over');
        }
      }
    }
  });
  $(gnbList).on("mouseleave", function () {
    $(this).closest(gnb).removeClass('js-open-d');
    $(logo).removeClass('js-open-d');
    $(siteMapBtn).removeClass('js-black');
    $(this).find('li').removeClass('js-open-d js-first');
    gnbState = 0;
  });

  $(".site-map-btn").on("click touchstart", function (e) {
    if (!$('body').hasClass('media-mobile')){
      if (!$(this).hasClass("js-open-d")) {
        $(this).addClass('js-open-d');
        $(siteMap).addClass('js-open-d');
        $(gnb).addClass('js-open-map');
      } else {
        $(this).removeClass('js-open-d');
        $(siteMap).removeClass('js-open-d');
        $(gnb).removeClass('js-open-map');
      }
      if(e.type === "touchstart") {
        return false;
      }
    } else {

    }
  });
});

// mobile menu
$(function () {
  $(document).on('click','.mobile-button-open, .mobile-button-close',function (e) {
    if ($('.mobile-menu').hasClass('is-on')) {
      $('.mobile-menu').removeClass('is-on');
      $('.mobile-menu-dimmed').remove();
    } else {
      $('.mobile-menu').addClass('is-on');
      $('<div class="mobile-menu-dimmed"></div>').appendTo($(document.body))
    }
  })
  $(window).resize(function () {
    if (!$('body').hasClass('media-mobile')) {
      $('.mobile-menu').removeClass('is-on');
      $('.mobile-menu-dimmed').remove();
      $('.mobile-button-open').attr('disabled','disabled');
      $('.mobile-button-close').attr('disabled','disabled');
    } else {
      $('.mobile-button-open').removeAttr('disabled');
      $('.mobile-button-close').removeAttr('disabled');
    }
  });
});

// mobile header fixed
$(function() {
  if ($('.site-header').length > 0) {
    $(window).on("scroll resize", function (e) {
      if (!$('body').hasClass('media-mobile')) {
        // headerFixed('.site-header');
        $('.mobile-header').removeClass('is-fixed');
      } else {
        headerFixed('.mobile-header');
        $('.site-header').removeClass('is-fixed');
      }
    });
    function headerFixed(target) {
      // console.log($(target)[0].offsetTop);
      if (!$(target).attr('data-top')) $(target).attr('data-top', $(target)[0].offsetTop);
      if ($(this).scrollTop() > $(target).attr('data-top')) {
        $(target).addClass('is-fixed');
      } else {
        $(target).removeClass('is-fixed');
      }
    }
  }
});

// mobile site map
$(function () {
  var siteMapitem = '.footer-site-map__depth1-item';
  var siteMapLink = '.footer-site-map__depth1-link';
  $(siteMapLink).on('click', function(e) {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')){
      if ($(this).closest(siteMapitem).find('ul').length == 0) return;
      e.preventDefault();
      if ($(this).closest(siteMapitem).hasClass('is-over')) {
        $(this).closest(siteMapitem).removeClass('is-over');
      } else {
        $(this).closest(siteMapitem).addClass('is-over');
      }
    }
  });
});

// selectbox
$(function () {
  var selectBox = '.js-selectbox';
  if($(selectBox).length > 0){
    $(selectBox).each(function () {
      $(this).niceSelect();
    });
  }
});

// tab
var tab = function() {
  if ($("[data-ui-tab='true']").length > 0) {
    $("[data-ui-tab='true']").each(function() {
      var $tab = $(this);
      var hash = window.location.hash;
      var tabArray= [];
      $tab.find("a[href^=#]").each(function(idx){
        tabArray.push($tab.find("a[href^=#]").eq(idx).attr("href"));
      });
      for (var i in tabArray) {
        $(tabArray[i]).removeClass("is-show").addClass("is-hide");
      }

      if (hash && tabArray.indexOf(hash) != -1) {
        sele($tab.find("a[href="+hash+"]"),"is-on","is-on");
        $(hash).removeClass("is-hide").addClass("is-show");
      }  else if ($tab.find("[data-ui-full]").length == 1 ) {
        sele($tab.find("a").eq(0),"is-on","is-on");
        for (var i in tabArray) $(tabArray[i]).removeClass("is-hide").addClass("is-show");
      } else {
        if ($tab.children().hasClass("is-on")) {
          $($tab.children(".is-on").find("a[href^=#]").attr("href")).removeClass("is-hide").addClass("is-show");
        } else {
          sele($tab.find("a[href^=#]").eq(0),"is-on","is-on");
          $(tabArray[0]).removeClass("is-hide").addClass("is-show");
        }
      }
    });
  }
  $(document).on("click","[data-ui-tab='true'] a[href^=#]",function(e){
    e.preventDefault();

    var $tab = $(this).closest("[data-ui-tab='true']");
    var tabArray= [];
    $tab.find("a[href^=#]").each(function(idx){
      tabArray.push($tab.find("a[href^=#]").eq(idx).attr("href"));
    });

    sele($(this),"is-on","is-on");
    if ($(this).data("ui-full") == true) {
      for (var i in tabArray) $(tabArray[i]).removeClass("is-hide").addClass("is-show");
    } else {
      if ($tab.data("ui-hash")) {
        if (!$(this).parent().hasClass("is-on")) window.location.hash = ($(this).attr("href"));
      }
      for (var i in tabArray) $(tabArray[i]).removeClass("is-show").addClass("is-hide");
      $($(this).attr("href")).removeClass("is-hide").addClass("is-show");
    }

    if (!$(e.target).hasClass("js-state") && $('.js-list').hasClass('is-open') && $(e.target).closest($('.js-list')).hasClass('is-open') == true) {
      $('.js-list').removeClass('is-open');
      $('.js-state').removeClass('is-open');
    }
  });
  function sele(el,show,hide) {
    $(el).parent().addClass(show).siblings().removeClass(hide);
  }
};
$(function() {
  tab();
});

// dropDown
$(function(){
  var $dropDown = $("[data-drop-down]");
  var $state = $dropDown.find(".js-state");
  var $list = $dropDown.find(".js-list");
  var toggoleOpen = "is-open";

  if($dropDown.find(".is-on").length > 0){
    var onTab = $dropDown.find(".is-on").children('a').text();
    $state.find('.u-tab2__state-name').html(onTab);
  }
  $state.on("click keypress",function(e){
    if ((e.keyCode == 13)||(e.type == "click")) {
      if ($(this).hasClass(toggoleOpen)) {
        $(this).removeClass(toggoleOpen).siblings($list).removeClass(toggoleOpen);
      } else {
        $(this).addClass(toggoleOpen).siblings($list).addClass(toggoleOpen);
      }
    }
  });
  $(document).on("click",function(e){
    if (!$(e.target).hasClass("js-state") && $list.hasClass(toggoleOpen) && $(e.target).closest($list).hasClass(toggoleOpen) == false) {
      $list.removeClass(toggoleOpen);
      $state.removeClass(toggoleOpen);
    }
    if($(e.target).hasClass('js-link') === true){
      $state.find('.u-tab2__state-name').html($(e.target).text());
    }
  });
});

// effect
$(function(){
  $('.js-view').each(function(){
    var $this = $(this);
    if ($this.offset().top < $(window).scrollTop() + $(window).height() && $this.offset().top > $(window).scrollTop() - $this.height()) {
      $this.addClass('is-view')
    } else {
      var n = function() {
        if ($this.offset().top < $(window).scrollTop() + $(window).height() && $this.offset().top > $(window).scrollTop() - $this.height()) {
          $this.addClass('is-view')
          $(window).unbind('scroll', n);
        }
      }
      $(window).bind('scroll', n);
      
    }
  });
});

// page location menu
$(function () {
  var breadcrumbItem = '.u-page-breadcrumb__item';
  var breadcrumbList = '.u-page-list';
  var breadcrumbShadow = '.u-page-breadcrumb__shadow';

  $(breadcrumbItem).on('mouseenter', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).not('.u-page-breadcrumb__item--home').addClass('is-over');
    $(breadcrumbShadow).fadeOut();
  });
  $(breadcrumbItem).on('mouseleave', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).removeClass('is-over');
    $(breadcrumbShadow).fadeOut();
  });
  $(breadcrumbItem).find('a').on('focusin', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).not('.u-page-breadcrumb__item--home').closest(breadcrumbItem).trigger('mouseenter');
  });
  $(breadcrumbItem).find('a:last').on('focusout', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).closest(breadcrumbItem).trigger('mouseleave');
  });
  $(breadcrumbItem).on('click', function(e) {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')){
      if ($(this).find(breadcrumbList).length == 0) return;
      if ($(this).hasClass('is-over')) {
        $(this).removeClass('is-over');
        $(breadcrumbShadow).fadeOut();
      } else {
        $(this).addClass('is-over');
        $(this).siblings().removeClass('is-over');
        $(breadcrumbShadow).fadeIn();
      }
    }

    if($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile') && $(this).hasClass('is-over')) {
      $('body, html').css('cursor', 'pointer');
    } else {
      $('body, html').attr('style','');
    }
  });
  $(document).on('click', function(e) {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) {
      if ($(e.target).siblings(breadcrumbItem).hasClass('is-over') == false && $(e.target).hasClass('u-page-list__link') == false && $(e.target).siblings(breadcrumbList).length == 0) {
        $(breadcrumbItem).removeClass('is-over');
        $(breadcrumbShadow).fadeOut();
        $('body, html').attr('style','');
      }
    }
  });
});

//layer popup
$(function () {
  var $popupBtn = $("[data-popup]");
  var $popupCloseBtn = $("[data-popup-close]");

  $popupBtn.on('click', function (event) {
    var $this = $(this);
    var element;
    if($this.attr('data-popup')){
      element = $this.attr('data-popup');
      layerPopup(element);
    }
  });

  $popupCloseBtn.on('click', function () {
    if($("body").attr("has-dimmed")){
      var element = $(this).attr('data-popup-close');
      $("body").removeAttr("has-dimmed");
      $('body').removeClass('is-not-scroll');
      $(".dimmed").fadeOut();
      $(element).fadeOut();
    }
  });

  function layerPopup(el) {
    if ($(el).length === 0) {
      alert("레이어팝업 없음");
      return false;
    }
    if(!$("body").attr("has-dimmed")){
      $("body").attr("has-dimmed",true);
      $('body').addClass('is-not-scroll');
      $(".dimmed").fadeIn();
      $(el).fadeIn();
    }
  }
});

// board 가로 스크롤
$(function () {
  var boardScroll = '.u-board-scroll__x';
  var boardFadeEl = '.u-board-scroll__fade';
  $(boardScroll).on('scroll', function() {
    var scrollLeft = $(this).scrollLeft();
    var boardWidth = $(this)[0].scrollWidth - $(this)[0].offsetWidth;
    if(scrollLeft === boardWidth){
      $(this).siblings(boardFadeEl).addClass('is-hide');
    } else {
      $(this).siblings(boardFadeEl).removeClass('is-hide');
    }
  });
});