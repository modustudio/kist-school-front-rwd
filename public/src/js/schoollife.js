// slide-mobile
$(window).on('resize', function () {
  if ($('body').hasClass('media-mobile')) {
    var $partnershipSlider = $('.js-partnership-slide-mobile');
    if ($partnershipSlider.find('.js-partnership-slide-item:not(.bx-clone)').length > 1) {
      var partnershipSlider = $partnershipSlider.bxSlider({
        wrapperClass: 'partnership-slide-wrap',
        minSlides: 1,
        maxSlides: 2,
        moveSlides: 1,
        slideMargin:15,
        slideWidth:1000,
        auto: false,
        speed: 500,
        pause: 6000,
        easing: 'ease-out',
        pager: true,
        controls: false,
        hideControlOnEnd: true,
        swipeThreshold: 100,
        onSliderResize: function (currentIndex) {
          if (!$('body').hasClass('media-mobile')) {
            partnershipSlider.destroySlider();
          }
        }
      });
    }
  }
});