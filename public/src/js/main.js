// visual
$(function() {
  var $mainVisualSlider = $('.js-main-visual');
  if( $mainVisualSlider.find('.js-main-visual-item:not(.bx-clone)').length > 1 ){
    var mainVisualSlider = $mainVisualSlider.bxSlider({
      wrapperClass: 'main-visual-wrap',
      auto: true,
      stopAutoOnClick:true,
      autoHover:true,
      pause: 5000,
      easing: 'ease-out',
      pager: true,
      controls: true,
      swipeThreshold: 100,
      responsive: true,
      onSliderLoad: function(currentIndex){
        $('.js-main-visual-item:not(.bx-clone)').eq(currentIndex).addClass('is-active');
        $('.js-main-visual-item:not(.bx-clone)').find('.js-view').removeClass('is-view');
        $('.js-main-visual-item:not(.bx-clone)').eq(currentIndex).find('.js-view').each(function () {
          var $this = $(this);
          $this.addClass('is-view');
        });
      },
      onSlideAfter: function($slideElement, oldIndex, newIndex){
        $('.js-main-visual-item:not(.bx-clone)').eq(newIndex).addClass('is-active');
        $slideElement.find('.js-view').each(function () {
          var $this = $(this);
          $this.addClass('is-view');
        });
      },
      onSliderResize: function (currentIndex) {
        mainVisualSlider.reloadSlider();
        $('.js-main-visual-item:not(.bx-clone)').removeClass('is-active');
      }
    });
  }
});

// main-mini-slide
$(function() {
  var $mainMiniSlider = $('.js-main-mini-slide');
  if( $mainMiniSlider.find('.js-main-mini-slide-item:not(.bx-clone)').length > 1 ){
    $mainMiniSlider.bxSlider({
      wrapperClass: 'main-mini-slide-wrap',
      auto: true,
      speed: 1000,
      pause: 6000,
      easing: 'ease-out',
      pager: true,
      controls: false,
      swipeThreshold: 100,
      responsive: true,
      onSliderLoad: function(currentIndex){
        $('.js-main-mini-slide-item:not(.bx-clone)').eq(currentIndex).addClass('is-active')
      },
      onSlideAfter: function($slideElement, oldIndex, newIndex){
        $slideElement.addClass('is-active').siblings().removeClass('is-active')
      }
    });
  }
});



// main notice
$(function() {
  $(window).on('resize', function () {
    if ($('body').hasClass('media-mobile')) {
      var $mainNoticeSlider = $('.js-main-notice-slide-mobile');
      if ($mainNoticeSlider.find('.js-main-notice-slide-item:not(.bx-clone)').length > 1) {
        var mainNoticeSlider = $mainNoticeSlider.bxSlider({
          wrapperClass: 'main-notice-slide-wrap',
          minSlides: 2,
          maxSlides: 2,
          moveSlides: 2,
          slideMargin:15,
          slideWidth:1000,
          auto: true,
          speed: 500,
          pause: 6000,
          easing: 'ease-out',
          pager: true,
          controls: false,
          hideControlOnEnd: true,
          swipeThreshold: 100,
          onSliderResize: function (currentIndex) {
            if (!$('body').hasClass('media-mobile')) {
              mainNoticeSlider.destroySlider();
            }
          }
        });
      }
    }
  });
});

// main news
$(function() {
  var $mainNewsTitleSlider = $('.js-main-news-title');
  var $mainNewsDateSlider = $('.js-main-news-date');
  if( $mainNewsTitleSlider.find('.js-main-news-title-item:not(.bx-clone)').length > 0 ){
    var mainNewsTitleSlider = $mainNewsTitleSlider.bxSlider({
      wrapperClass: 'main-news__component main-news-title-wrap',
      auto: true,
      mode: 'vertical',
      easing: 'ease-out',
      pager: false,
      controls: false,
      responsive: true,
      touchEnabled: false,
      onSliderResize: function (currentIndex) {
        mainNewsTitleSlider.reloadSlider();
      }
    });
    var mainNewsDateSlider = $mainNewsDateSlider.bxSlider({
      wrapperClass: 'main-news__component main-news-date-wrap',
      auto: true,
      mode: 'vertical',
      easing: 'ease-out',
      pager: false,
      controls: false,
      responsive: true,
      touchEnabled: false,
      onSliderResize: function (currentIndex) {
        mainNewsDateSlider.reloadSlider();
      }
    });
    $('.js-main-news-prev').bind('click', function(e){
      e.preventDefault();
      mainNewsTitleSlider.stopAuto();
      mainNewsTitleSlider.stopAuto();
      mainNewsTitleSlider.goToPrevSlide();
      mainNewsDateSlider.goToPrevSlide();
    });
    $('.js-main-news-next').bind('click', function(e){
      e.preventDefault();
      mainNewsTitleSlider.stopAuto();
      mainNewsDateSlider.stopAuto();
      mainNewsTitleSlider.goToNextSlide();
      mainNewsDateSlider.goToNextSlide();
    });
  }
});

// major
$(function () {
  $(window).on('resize', function () {
    if ($('body').hasClass('media-mobile')) {
      var $mainMajorSlider = $('.js-main-major-slide-mobile');
      if ($mainMajorSlider.find('.js-main-major-slide-item:not(.bx-clone)').length > 1) {
        var mainMajorSlider = $mainMajorSlider.bxSlider({
          wrapperClass: 'main-major-slide-wrap',
          minSlides: 1,
          maxSlides: 2,
          moveSlides: 1,
          slideMargin:0,
          slideWidth:580,
          auto: false,
          speed: 500,
          pause: 6000,
          easing: 'ease-out',
          pager: true,
          controls: true,
          swipeThreshold: 100,
          onSliderResize: function (currentIndex) {
            if (!$('body').hasClass('media-mobile')) {
              mainMajorSlider.destroySlider();
            }
          }
        });
      }
    }

    var $majorList = $('.main-major__list');
    var $majorItem = $majorList.find('.main-major__item');
    if (!$('body').hasClass('media-mobile')) {
      $majorItem.bind('mouseenter', function () {
        $(this).addClass('is-over');
      });
      $majorItem.bind('mouseleave', function () {
        $(this).removeClass('is-over');
      });
    } else {
      $majorItem.unbind("mouseenter mouseleave");
    }

    var $galleryList = $('.main-gallery__list');
    var $galleryItem = $galleryList.find('.main-gallery__item');
    if (!$('body').hasClass('media-mobile')) {
      $galleryItem.bind('mouseenter', function () {
        $(this).addClass('is-over');
      });
      $galleryItem.bind('mouseleave', function () {
        $(this).removeClass('is-over');
      });
    } else {
      $galleryItem.unbind("mouseenter mouseleave");
    }
  });
});