// slide-mobile
$(window).on('resize', function () {
  if ($('body').hasClass('media-mobile')) {
    var $kistschoolSlider = $('.js-kistschool-slide-mobile');
    if ($kistschoolSlider.find('.js-kistschool-slide-item:not(.bx-clone)').length > 1) {
      var kistschoolSlider = $kistschoolSlider.bxSlider({
        wrapperClass: 'kistschool-slide-wrap',
        minSlides: 1,
        maxSlides: 2,
        moveSlides: 1,
        slideMargin:15,
        slideWidth:1000,
        auto: false,
        speed: 500,
        pause: 6000,
        easing: 'ease-out',
        pager: true,
        controls: false,
        hideControlOnEnd: true,
        swipeThreshold: 100,
        onSliderResize: function (currentIndex) {
          if (!$('body').hasClass('media-mobile')) {
            kistschoolSlider.destroySlider();
          }
        }
      });
    }
    
    var $statusSlider = $('.js-status-slide-mobile');
    if ($statusSlider.find('.js-status-slide-item:not(.bx-clone)').length > 1) {
      var statusSlider = $statusSlider.bxSlider({
        wrapperClass: 'status-slide-wrap',
        minSlides: 1,
        maxSlides: 2,
        moveSlides: 1,
        slideMargin:15,
        slideWidth:1000,
        auto: false,
        speed: 500,
        pause: 6000,
        easing: 'ease-out',
        pager: true,
        controls: false,
        hideControlOnEnd: true,
        swipeThreshold: 100,
        onSliderResize: function (currentIndex) {
          if (!$('body').hasClass('media-mobile')) {
            statusSlider.destroySlider();
          }
        }
      });
    }
  }
});

$(function () {
  var $viewItem = $('.js-view-item');
  var $viewMoreBtn = $('.js-view-more-btn');
  if($viewItem.length > 0){
    $viewItem.slice(0, 7).addClass('is-show');
    $viewMoreBtn.on('click', function (e) {
      e.preventDefault();
      $('.js-view-item:not(".is-show")').slice(0, 4).addClass('is-show');
      if ($('.js-view-item:not(".is-show")').length === 0) {
        $viewMoreBtn.fadeOut('fast');
      }
      $('html,body').animate({
        scrollTop: $(this).offset().top
      }, 500);
    });
  }
});