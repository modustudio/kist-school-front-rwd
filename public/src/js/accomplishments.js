$(function () {
  var $viewList = $('.js-view-list');
  var $viewItem = $('.js-view-item');
  var $viewMoreBtn = $('.js-view-more-btn');
  var itemMiddle;
  $(window).on('resize', function () {
    $viewItem.removeClass('is-show');
    $viewMoreBtn.fadeIn();
    if (!$('body').hasClass('media-mobile')) {
      if ($viewItem.length > 0) {
        $viewList.each(function () {
          itemMiddle = $(this).find('.js-view-item').length / 2;
          var $thisItem = $(this).find('.js-view-item');
          $thisItem.slice(0, itemMiddle).addClass('is-show');
          $viewMoreBtn.on('click', function (e) {
            e.preventDefault();
            $('.js-view-item:not(".is-show")').slice(0, itemMiddle).addClass('is-show');
            if ($('.js-view-item:not(".is-show")').length === 0) {
              $viewMoreBtn.fadeOut('fast');
            }
          });
        });
      }
    } else {
      itemMiddle = $viewItem.length / 2;
      $viewItem.slice(0, itemMiddle).addClass('is-show');
      $viewMoreBtn.on('click', function (e) {
        e.preventDefault();
        $('.js-view-item:not(".is-show")').slice(0, itemMiddle).addClass('is-show');
        if ($('.js-view-item:not(".is-show")').length === 0) {
          $viewMoreBtn.fadeOut('fast');
        }
      });
    }
  })
});

$(function () {
  $(window).on('resize', function () {
    if ($('body').hasClass('media-mobile')) {
      var $researchSlider = $('.js-research-performance-slide-mobile');
      if ($researchSlider.find('.js-research-performance-slide-item:not(.bx-clone)').length > 1) {
        var researchSlider = $researchSlider.bxSlider({
          wrapperClass: 'research-slide-wrap',
          auto: false,
          speed: 500,
          pause: 6000,
          easing: 'ease-out',
          pager: false,
          controls: true,
          swipeThreshold: 100,
          onSliderResize: function (currentIndex) {
            if (!$('body').hasClass('media-mobile')) {
              researchSlider.destroySlider();
            }
          }
        });
      }
    }
  });
});